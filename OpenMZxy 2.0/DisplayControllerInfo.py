__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-

import tkMessageBox
import Variables


def displaycontrollerinfo():
    """"""
    advancedservo = Variables.advancedservo

    tkMessageBox.showinfo("Display Controller Info", "\nServo controller %d attached!" % advancedservo.getSerialNum() +
                          "\n\nAttached : %s" % advancedservo.isAttached() +
                          "\nType : %s " % advancedservo.getDeviceName() +
                          "\nSerial No. : %d" % advancedservo.getSerialNum() +
                          "\nVersion : %d" % advancedservo.getDeviceVersion() +
                          "\nNumber of motors: %i" % advancedservo.getMotorCount())