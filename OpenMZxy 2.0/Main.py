__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-\

#    OpenMZxy is a software for controlling an imaging/sampling system based
#    on Phidgets. A PhidgetAdvancedServo 8-Motor controller, and a Joystick
#    (of good quality, such as a flight simulation joystick) need to be 
#    connected for the function of the program. 2D movement of a sample carrier
#    is implemented and requires 2 Linear Actuator L12-100-100-06-R (Phidgets Inc.).

#    Copyright (C) 2013-2015 Dr. Robert Winkler and Contributors 
#    email: robert.winkler@ira.cinvestav.mx, robert.winkler@bioprocess.org 
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx

#    Contributor(s): Pedro Jiménez-Sandoval, Angel Velasco

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from DisplayControllerInfo import *
from TestMotors import *
from ManualControl import *
from helpabout import *
import Variables
import automaticposition

# Main Window
root = Tk()
root.title("OpenMZxy 2.0")
root.wm_iconbitmap("lab1.ico")
root.geometry("+140+90")
root.resizable(0, 0)
Variables.frame = Frame(root)
Variables.frame.pack()
Variables.frame.grid(sticky=W)
# INPUT area

header_label = Label(Variables.frame, text="INPUT")
header_label.grid(row=0, column=1, sticky=W)

timer_label = Label(Variables.frame, text="Scan time [s]:")
timer_label.grid(row=1, column=0, sticky=W)
timervalue_1 = DoubleVar()
timervalue_1.set(Variables.timervalue_1)
timervalue_entry = Entry(Variables.frame, textvariable=timervalue_1)
timervalue_entry.grid(row=1, column=1, sticky=W)

interscan_label = Label(Variables.frame, text="Inter-scan time [s]:")
interscan_label.grid(row=2, column=0, sticky=W)
timeinterscan_1 = DoubleVar()
timeinterscan_1.set(Variables.timeinterscan_1)
timeinterscan_entry = Entry(Variables.frame, textvariable=timeinterscan_1)
timeinterscan_entry.grid(row=2, column=1, sticky=W)

stepperx_label = Label(Variables.frame, text="Resolution x-axis [mm]:")
stepperx_label.grid(row=3, column=0, sticky=W)
steppervalue_x = DoubleVar()
steppervalue_x.set(Variables.steppervalue_x)
steppervalue_entry = Entry(Variables.frame, textvariable=steppervalue_x)
steppervalue_entry.grid(row=3, column=1, sticky=W)

steppery_label = Label(Variables.frame, text="Resolution y-axis [mm]:")
steppery_label.grid(row=4, column=0, sticky=W)
steppervalue_y = DoubleVar()
steppervalue_y.set(Variables.steppervalue_y)
steppervalue_entry = Entry(Variables.frame, textvariable=steppervalue_y)
steppervalue_entry.grid(row=4, column=1, sticky=W)

samplingx_label = Label(Variables.frame, text="Sampling x-axis (to the right) [mm]:")
samplingx_label.grid(row=5, column=0, sticky=W)
samplingxvalue = DoubleVar()
samplingxvalue.set(Variables.samplingxvalue)
samplingxvalue_entry = Entry(Variables.frame, textvariable=samplingxvalue)
samplingxvalue_entry.grid(row=5, column=1, sticky=W)

samplingy_label = Label(Variables.frame, text="Sampling y-axis (downwards) [mm]:")
samplingy_label.grid(row=6, column=0, sticky=W)
samplingyvalue = DoubleVar()
samplingyvalue.set(Variables.samplingyvalue)
samplingyvalue_entry = Entry(Variables.frame, textvariable=samplingyvalue)
samplingyvalue_entry.grid(row=6, column=1, sticky=W)

# OUTPUT area

header_label = Label(Variables.frame, text="OUTPUT")
header_label.grid(row=7, column=1, sticky=W)

posfilename_label = Label(Variables.frame, text="t,x,y file:")
posfilename_label.grid(row=8, column=0, sticky=W)
posfilename_value = StringVar()
posfilename_value.set(Variables.posfilename_value)
posfilename_result = Entry(Variables.frame, textvariable=posfilename_value)
posfilename_result.grid(row=8, column=1, sticky=W)

posxget_label = Label(Variables.frame, text="Current position x: ")
posxget_label.grid(row=9, column=0, sticky=W)
posxget_value = DoubleVar()
posxget_value.set(Variables.servoposx)
posxget_result = Entry(Variables.frame, textvariable=posxget_value)
posxget_result.grid(row=9, column=1, sticky=W)

posyget_label = Label(Variables.frame, text="Current position y: ")
posyget_label.grid(row=10, column=0, sticky=W)
posyget_value = DoubleVar()
posyget_value.set(Variables.servoposy)
posyget_result = Entry(Variables.frame, textvariable=posyget_value)
posyget_result.grid(row=10, column=1, sticky=W)

samplingarea_label = Label(Variables.frame, text="Sampling Area (x1,y1 : x2,y2): ")
samplingarea_label.grid(row=11, column=0, sticky=W)
samplingarea_value = DoubleVar()
samplingarea_value.set(Variables.samplingarea_x1y1x2y2)
samplingarea_result = Entry(Variables.frame, textvariable=samplingarea_value, width=25)
samplingarea_result.grid(row=11, column=1, sticky=W)

timecheck = IntVar()
timecheckbox = Checkbutton(Variables.frame, text="Include time [ms] in  .dat file", variable=timecheck, onvalue=1,
                           offvalue=0, height=2)
timecheckbox.grid(row=12, column=0, sticky=W)
timecheckbox.select()

s = ttk.Separator(Variables.frame, orient=HORIZONTAL)
s.grid(row=17, columnspan=2, sticky=EW)

Variables_label = Label(Variables.frame, text="Servo controller %d attached!" % Variables.advancedservo.getSerialNum())
Variables_label.grid(row=18, column=0, sticky=W)
Variables_label = Label(Variables.frame, text="Setting the servo type for motors 0 and 7 to FIRGELLI_L12_100_100_06_R")
Variables_label.grid(row=19, column=0, sticky=W)
Variables_label = Label(Variables.frame, text="Engaged state motor x: %s" % Variables.advancedservo.getEngaged(0))
Variables_label.grid(row=20, column=0, sticky=W)
Variables_label = Label(Variables.frame, text="Engaged state motor y: %s" % Variables.advancedservo.getEngaged(7))
Variables_label.grid(row=21, column=0, sticky=W)

menu = Menu(root)
root.config(menu=menu)

samplecarriermenu = Menu(menu)
samplecarriermenu = Menu(samplecarriermenu, tearoff=0)
menu.add_cascade(label="Sample Carrier", menu=samplecarriermenu)
samplecarriermenu.add_command(label="Display Controller Info", command=displaycontrollerinfo)
samplecarriermenu.add_command(label="Test Full Range of Motors", command=TestMotors)
samplecarriermenu1_1 = Menu(menu, tearoff=0)
samplecarriermenu.add_separator()
samplecarriermenu.add_cascade(label="Control Manual", menu=samplecarriermenu1_1)


def joysticksampling():
    JoystickControl.j_samplingx = samplingxvalue.get()
    JoystickControl.j_samplingy = samplingyvalue.get()

samplecarriermenu1_1.add_command(label="Joystick Control", command=lambda: {joysticksampling(), manualcontrol()})


def keysampling():
    Keyboardcontrol.key_samplingx = samplingxvalue.get()
    Keyboardcontrol.key_samplingy = samplingyvalue.get()

samplecarriermenu1_1.add_command(label="Keyboard Control", command=lambda: {keysampling(), key()})
samplecarriermenu1_1.add_command(label="Send Position", command=lambda: {automaticposition.automatic()})

samplecarriermenu.add_separator()


def rectangularinput():
    Rectangularsampling.r_timervalue = timervalue_1.get()
    Rectangularsampling.r_timeinterscan = timeinterscan_1.get()
    Rectangularsampling.r_steppervalue_x = steppervalue_x.get()
    Rectangularsampling.r_steppervalue_y = steppervalue_y.get()
    Rectangularsampling.r_samplingx = samplingxvalue.get()
    Rectangularsampling.r_samplingy = samplingyvalue.get()
    Rectangularsampling.r_timecheck = timecheck.get()

samplecarriermenu.add_command(label="Rectangular Sampling (auto)", command=lambda: {rectangularinput(), rectangular()})


helpmenu = Menu(menu)
helpmenu = Menu(helpmenu, tearoff=0)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="About", command=help_about)

root.mainloop()
