__author__ = 'Angel Velasco'
from Tkinter import *
import Variables


def automatic():

    posxget_value = DoubleVar()
    posxget_value.set(Variables.advancedservo.getPosition(0))
    posxget_result = Entry(Variables.frame, textvariable=posxget_value)
    posxget_result.grid(row=9, column=1, sticky=W)

    posyget_value = DoubleVar()
    posyget_value.set(100 - Variables.advancedservo.getPosition(7))
    posyget_result = Entry(Variables.frame, textvariable=posyget_value)
    posyget_result.grid(row=10, column=1, sticky=W)

    headerlabel = Label(Variables.frame, text="Automatic Position")
    headerlabel.grid(row=13, column=1, sticky=W)

    automaticpositionx_label = Label(Variables.frame, text="Automatic Position X:")
    automaticpositionx_label.grid(row=14, column=0, sticky=W)
    automaticpositionx_value = DoubleVar()
    automaticpositionx_value.set(Variables.advancedservo.getPosition(0))
    automaticpositionx_result = Entry(Variables.frame, textvariable=automaticpositionx_value)
    automaticpositionx_result.grid(row=14, column=1, sticky=W)

    automaticpositiony_label = Label(Variables.frame, text="Automatic Position Y:")
    automaticpositiony_label.grid(row=15, column=0, sticky=W)
    automaticpositiony_value = DoubleVar()
    automaticpositiony_value.set(100 - Variables.advancedservo.getPosition(7))
    automaticpositiony_result = Entry(Variables.frame, textvariable=automaticpositiony_value)
    automaticpositiony_result.grid(row=15, column=1, sticky=W)

    automaticposition_label = Button(Variables.frame, text="       Send Position       ", command=lambda: {sent()})
    automaticposition_label.grid(row=16, column=1, sticky=W)

    def sent():

        Variables.advancedservo.setPosition(0, automaticpositionx_value.get())
        Variables.advancedservo.setPosition(7, (-automaticpositiony_value.get() + 100))
        posxget_value.set(automaticpositionx_value.get())
        posyget_value.set(automaticpositiony_value.get())
        headerlabel.destroy()
        automaticposition_label.destroy()
        automaticpositionx_label.destroy()
        automaticpositionx_result.destroy()
        automaticpositiony_label.destroy()
        automaticpositiony_result.destroy()