__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-

from Tkinter import *
import tkMessageBox
import Variables
import pygame
import ttk


class JoystickControl():
    j_samplingy = 0.0
    j_samplingx = 0.0

    def __init__(self, joystick_axis=None, ):

        self.joystickpositionx = None
        self.joystickpositiony = None
        self.joystick_axis = joystick_axis

        servoposx = float(Variables.advancedservo.getPosition(0))
        servoposy = float(100 - Variables.advancedservo.getPosition(7))

        # OUTPUT area
        posxget_value = DoubleVar()
        posxget_value.set(Variables.advancedservo.getPosition(0))
        posxget_result = Entry(Variables.frame, textvariable=posxget_value)
        posxget_result.grid(row=9, column=1, sticky=W)

        posyget_value = DoubleVar()
        posyget_value.set(Variables.advancedservo.getPosition(7))
        posyget_result = Entry(Variables.frame, textvariable=posyget_value)
        posyget_result.grid(row=10, column=1, sticky=W)

        samplingarea_value = DoubleVar()
        samplingarea_value.set(Variables.samplingarea_x1y1x2y2)
        samplingarea_result = Entry(Variables.frame, textvariable=samplingarea_value, width=25)
        samplingarea_result.grid(row=11, column=1, sticky=W)

        canvas = Canvas(width=500, height=500, bg='white')
        canvas.pack()
        canvas.grid(row=0, column=2, sticky=E)

        canvas.create_line(0, 0, 0, 505, fill="#C8C8C8", width=5)
        canvas.create_line(0, 0, 505, 0, fill="#C8C8C8", width=5)
        canvas.create_line(503, 0, 503, 503, fill="#C8C8C8", width=5)
        canvas.create_line(0, 503, 503, 503, fill="#C8C8C8", width=5)

        s = ttk.Separator(Variables.frame, orient=HORIZONTAL)
        s.grid(row=22, columnspan=2, sticky=EW)

        samplingx = JoystickControl.j_samplingx
        samplingy = JoystickControl.j_samplingy

        joystickcursorx_1 = 5 * servoposx
        joystickcursory_1 = 5 * servoposy

        variables1_label = Label(Variables.frame, text="Servopos X,Y, Cursor X1, X2: " + str(servoposx) + ", " + str(
            servoposy) + ", " + str(joystickcursorx_1) + ", " + str(joystickcursory_1))
        variables1_label.grid(row=23, column=0, sticky=W)

        opointcorrectionx = 0
        opointcorrectiony = 0
        dynamicscorrection = 3

        tkMessageBox.showinfo('Joystick Control', "Lock position by pressing any Joystick-Button")
        joystick_axis = pygame.joystick.Joystick(0)
        joystick_axis.init()

        while True:
            pytime = pygame.time.get_ticks()
            variables4_label = Label(Variables.frame, text="PyGameTime: " + str(pytime) + " ms")
            variables4_label.grid(row=26, column=0, sticky=W)

            joystickpositionx = (joystick_axis.get_axis(0) + opointcorrectionx) * dynamicscorrection
            joystickpositionx = round(joystickpositionx, 5)
            joystickpositiony = (joystick_axis.get_axis(1) + opointcorrectiony) * dynamicscorrection
            joystickpositiony = round(joystickpositiony, 5)

            variables2_label = Label(Variables.frame,
                                     text="Using calibrataion set X: " + str(joystick_axis.get_axis(0)))
            variables2_label.grid(row=24, column=0, sticky=W)
            variables3_label = Label(Variables.frame,
                                     text="Using calibrataion set Y: " + str(joystick_axis.get_axis(1)))
            variables3_label.grid(row=25, column=0, sticky=W)

            variables5_label = Label(Variables.frame,
                                     text="Joystick Postition X,Y: " + str(joystickpositionx) + ", " + str(
                                         joystickpositiony))
            variables5_label.grid(row=27, column=0, sticky=W)

            joystickcursorx_2 = joystickcursorx_1
            joystickcursory_2 = joystickcursory_1
            joystickcursorx_1 += joystickpositionx
            joystickcursory_1 += joystickpositiony

            joystickservoposx = joystickcursorx_2 / 5
            if joystickservoposx < 1:
                joystickservoposx = 1
            if joystickservoposx > 100:
                joystickservoposx = 100
            Variables.advancedservo.setPosition(0, joystickservoposx)
            joystickservoposy = 100 - (joystickcursory_2 / 5)
            if joystickservoposy < 1:
                joystickservoposy = 1
            if joystickservoposy > 100:
                joystickservoposy = 100
            Variables.advancedservo.setPosition(7, joystickservoposy)

            canvas.create_line(joystickcursorx_1, joystickcursory_1, joystickcursorx_2, joystickcursory_2, width=1,
                               fill="#FF0505")

            posxget_value.set(Variables.advancedservo.getPosition(0))
            posyget_value.set(100 - Variables.advancedservo.getPosition(7))

            samplingareax2 = (Variables.advancedservo.getPosition(0)) + samplingx
            samplingareay2 = (100 - Variables.advancedservo.getPosition(7)) + samplingy
            samplingarea_x1y1x2y2 = str(Variables.advancedservo.getPosition(0)) + "," + str(
                100 - Variables.advancedservo.getPosition(7)) + ":" + str(samplingareax2) + "," + str(samplingareay2)

            samplingarea_value.set(samplingarea_x1y1x2y2)

            canvas.update()

            variables1_label.destroy()
            variables2_label.destroy()
            variables3_label.destroy()
            variables4_label.destroy()
            variables5_label.destroy()

            # Loop until Joystick button is pressed

            e = pygame.event.wait()
            if e.type == pygame.JOYBUTTONDOWN:
                tkMessageBox.showinfo('Joystick Control', "Button pressed, position locked!")

                canvas.destroy()
                s.destroy()
                variables1_label.destroy()
                variables2_label.destroy()
                variables3_label.destroy()
                variables4_label.destroy()
                variables5_label.destroy()

                return

            if e.type == pygame.JOYAXISMOTION:
                continue