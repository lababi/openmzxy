__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-

import tkMessageBox


def help_about():
    """"""

    return tkMessageBox.showinfo("\nAbout",
                                 "\nOpenMZxy 2.0 "
                                 "\n\nLicense: GPLv3 "
                                 "\n\nDevelopers :"
                                 "\n\nRobert Winkler, Pedro Jiménez-Sandoval, Velasco Sánchez Luis Angel"
                                 "\nCINVESTAV, Irapuato, México, 2015"
                                 "\n\nhttp://www.lababi.bioprocess.org/"
                                 "\nhttp://www.ira.cinvestav.mx/lababi.aspx")