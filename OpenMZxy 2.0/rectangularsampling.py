__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-

from Tkinter import *
from time import sleep
from time import strftime
import ttk
import tkMessageBox
import numpy
import pygame
import Variables


class Rectangularsampling():
    r_timervalue = 0.0
    r_timeinterscan = 0.0
    r_steppervalue_x = 0.0
    r_steppervalue_y = 0.0
    r_samplingy = 0.0
    r_samplingx = 0.0
    r_timecheck = 0

    def __init__(self):
        """"""

        global posxsetting, xyoutput
        # OUTPUT
        posfilename_value = StringVar()
        posfilename_value.set(Variables.posfilename_value)
        posfilename_result = Entry(Variables.frame, textvariable=posfilename_value)
        posfilename_result.grid(row=8, column=1, sticky=W)

        posxget_value = DoubleVar()
        posxget_value.set(Variables.advancedservo.getPosition(0))
        posxget_result = Entry(Variables.frame, textvariable=posxget_value)
        posxget_result.grid(row=9, column=1, sticky=W)

        posyget_value = DoubleVar()
        posyget_value.set(Variables.advancedservo.getPosition(7))
        posyget_result = Entry(Variables.frame, textvariable=posyget_value)
        posyget_result.grid(row=10, column=1, sticky=W)

        samplingarea_value = DoubleVar()
        samplingarea_value.set(Variables.samplingarea_x1y1x2y2)
        samplingarea_result = Entry(Variables.frame, textvariable=samplingarea_value, width=25)
        samplingarea_result.grid(row=11, column=1, sticky=W)

        canvas = Canvas(width=500, height=500, bg='white')
        canvas.pack()
        canvas.grid(row=0, column=2, sticky=E)

        canvas.create_line(0, 0, 0, 505, fill="#C8C8C8", width=5)
        canvas.create_line(0, 0, 505, 0, fill="#C8C8C8", width=5)
        canvas.create_line(503, 0, 503, 503, fill="#C8C8C8", width=5)
        canvas.create_line(0, 503, 503, 503, fill="#C8C8C8", width=5)
        pygame.init()

        s = ttk.Separator(Variables.frame, orient=HORIZONTAL)
        s.grid(row=22, columnspan=2, sticky=EW)

        servoposx = int(Variables.advancedservo.getPosition(0))
        servoposy = int(100 - Variables.advancedservo.getPosition(7))

        samplingcursorx_1 = 5 * servoposx
        samplingcursory_1 = 5 * servoposy

        samplingx = Rectangularsampling.r_samplingx
        samplingy = Rectangularsampling.r_samplingy

        samplingareax2 = servoposx + samplingx
        samplingareay2 = servoposy + samplingy
        samplingarea_x1y1x2y2 = str(servoposx) + "," + str(servoposy) + ":" + str(samplingareax2) + "," + str(
            samplingareay2)

        samplingarea_value.set(samplingarea_x1y1x2y2)

        samplingboxx_1 = samplingcursorx_1
        samplingboxy_1 = samplingcursory_1
        samplingboxx_2 = 5 * samplingareax2
        samplingboxy_2 = 5 * samplingareay2

        canvas.create_rectangle(samplingboxx_1, samplingboxy_1, samplingboxx_2, samplingboxy_2, fill="#000F83")
        canvas.update()

        minx = servoposx
        maxx = servoposx + samplingx

        if maxx > 95:
            # Accuracy Range
            maxx = 95

        stepperx = int(Rectangularsampling.r_steppervalue_x)

        miny = servoposy
        maxy = servoposy + samplingy

        if maxy > 95:
            # Accuracy Range
            maxy = 95

        steppery = int(Rectangularsampling.r_steppervalue_y)

        maxx += stepperx
        maxy += steppery
        # start logging coordinates
        posfilename = strftime("%Y%m%d_%H%M%S") + ".dat"
        posfile = open(posfilename, "a")
        posfilename_value.set(posfilename)

        pynextpermittime = (Rectangularsampling.r_timervalue + Rectangularsampling.r_timeinterscan) * 1000

        sleeptimenew = Rectangularsampling.r_timervalue + Rectangularsampling.r_timeinterscan
        truetime = pygame.time.get_ticks() / 1000

        xdirection = -1
        pygame.init()

        for ycounter in numpy.arange(miny, maxy, steppery):
            posysetting = 100 - ycounter

            Variables.advancedservo.setPosition(7, posysetting)

            servoposx = int(Variables.advancedservo.getPosition(0))
            servoposy = int(100 - Variables.advancedservo.getPosition(7))

            samplingcursorx_2 = 5 * servoposx
            samplingcursory_2 = 5 * servoposy
            canvas.create_line(samplingcursorx_1, samplingcursory_1, samplingcursorx_2, samplingcursory_2, width=1,
                               fill="#0D8300")
            samplingcursorx_1 = samplingcursorx_2
            samplingcursory_1 = samplingcursory_2

            xdirection *= -1

            posxget_value.set(Variables.advancedservo.getPosition(0))
            posyget_value.set(100 - Variables.advancedservo.getPosition(7))
            canvas.update()

            for xcounter in numpy.arange(minx, maxx, stepperx):
                if xdirection > 0:
                    posxsetting = xcounter
                if xdirection < 0:
                    posxsetting = maxx - (xcounter - minx + stepperx)

                Variables.advancedservo.setPosition(0, posxsetting)
                variables3_label = Label(Variables.frame, text="Next SleepTime: " + str(sleeptimenew))
                variables3_label.grid(row=23, column=0, sticky=W)

                sleep(sleeptimenew)

                servoposx = int(Variables.advancedservo.getPosition(0))
                servoposy = int(100 - Variables.advancedservo.getPosition(7))

                if Rectangularsampling.r_timecheck == 1:
                    xyoutput = str(pygame.time.get_ticks() - truetime * 1000) + "\t" + str(servoposx) + "\t" + str(
                        servoposy) + "\n"
                if Rectangularsampling.r_timecheck == 0:
                    xyoutput = str(servoposx) + "\t" + str(servoposy) + "\n"

                posfile.write(xyoutput)
                samplingcursorx_2 = 5 * servoposx
                samplingcursory_2 = 5 * servoposy
                canvas.create_line(samplingcursorx_1, samplingcursory_1, samplingcursorx_2, samplingcursory_2, width=1,
                                   fill="#FF0505")
                samplingcursorx_1 = samplingcursorx_2
                samplingcursory_1 = samplingcursory_2

                posxget_value.set(Variables.advancedservo.getPosition(0))
                posyget_value.set(100 - Variables.advancedservo.getPosition(7))

                variables4_label = Label(Variables.frame, text="Current Time: " + str(pygame.time.get_ticks() / 1000))
                variables4_label.grid(row=24, column=0, sticky=W)

                pynextpermittime += (Rectangularsampling.r_timervalue + Rectangularsampling.r_timeinterscan) * 1000
                # Time to Next Position

                sleeptimenew = (pynextpermittime / 1000 - pygame.time.get_ticks() / 1000 + truetime)

                variables5_label = Label(Variables.frame, text="Next Permit Time: " + str(pynextpermittime / 1000))
                variables5_label.grid(row=25, column=0, sticky=W)

                canvas.update()
                variables3_label.destroy()
                variables4_label.destroy()
                variables5_label.destroy()

        print("Ready!")
        readymessage = "Positioning data have been recorded in " + posfilename
        tkMessageBox.showinfo('Ready!', readymessage)
        posfile.close()
        canvas.destroy()
        s.destroy()