__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-
from Tkinter import *
import tkMessageBox
import Variables
import ttk


def TestMotors():

    s = ttk.Separator(Variables.frame, orient=HORIZONTAL)
    s.grid(row=22, columnspan=2, sticky=EW)

    variables_label1 = Label(Variables.frame, text="Move motors to position PositionMin: ")
    variables_label1.grid(row=23, column=0, sticky=W)

    Variables.advancedservo.setPosition(0, Variables.advancedservo.getPositionMin(0))
    Variables.advancedservo.setPosition(7, Variables.advancedservo.getPositionMin(7))
    Variables.sleep(10)

    variables_label2 = Label(Variables.frame, text="PositionMin reached!")
    variables_label2.grid(row=24, column=0, sticky=W)
    variables_label3 = Label(Variables.frame, text="Move motors to position PositionMax...")
    variables_label3.grid(row=25, column=0, sticky=W)

    Variables.advancedservo.setPosition(0, Variables.advancedservo.getPositionMax(0))
    Variables.advancedservo.setPosition(7, Variables.advancedservo.getPositionMax(7))
    Variables.sleep(5)
    variables_label4 = Label(Variables.frame, text="PositionMax reached!")
    variables_label4.grid(row=26, column=0, sticky=W)

    tkMessageBox.showinfo("Test Motors Info", "\nTest Motors is complete")

    s.destroy()
    variables_label4.destroy()
    variables_label3.destroy()
    variables_label1.destroy()
    variables_label2.destroy()
