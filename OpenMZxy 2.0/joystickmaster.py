__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-

import pygame
import tkMessageBox


class Joystickmaster():
    def __init__(self):
        global pyTime
        joystick_input = pygame.joystick.Joystick(0)
        joystick_input.init()
        pyTime = pygame.time.get_ticks()

        tkMessageBox.showinfo("Joystick Controller Input",
                              "\nNumber of Joysticks: " + str(pygame.joystick.get_count()) +
                              "\nJoystick 1, Initialised? " + str(joystick_input.get_init()) +
                              "\nJoystick 1, Name: " + str(joystick_input.get_name()) +
                              "\nJoystick 1, Number of Axes: " + str(joystick_input.get_numaxes()) +
                              "\nJoystick 1, Number of Balls: " + str(joystick_input.get_numballs()) +
                              "\nJoystick 1, Number of Buttos: " + str(joystick_input.get_numbuttons()) +
                              "\nJoystick 1, Number of Hats: " + str(joystick_input.get_numhats()) +
                              "\nJoystick Postition X,Y: " + str(joystick_input.get_axis(0)) + " , " + str(
                                  joystick_input.get_axis(1)) + "\nPyGameTime: " + str(pygame.time.get_ticks()) + "ms"
                              )