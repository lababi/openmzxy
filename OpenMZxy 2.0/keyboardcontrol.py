__author__ = 'Angel Velasco'
# -*- coding: utf-8 -*-

from Tkinter import *
import tkMessageBox
import Variables
import pygame


class Keyboardcontrol():
    key_samplingx = 0
    key_samplingy = 0

    def __init__(self):

        pygame.init()

        window = pygame.display.set_mode((720, 500))
        pygame.display.set_caption("OpenMZxy 2.0")
        icon = pygame.image.load("lab1.png")
        pygame.display.set_icon(icon)
        fuente = pygame.font.SysFont('Consolas', 12)
        green = (0, 15, 131)
        white = (255, 255, 255)
        grey = (242, 242, 242)
        black = (200, 200, 200)

        tkMessageBox.showinfo("Keyboard Control", "Press the space bar to exit the Keyboard Control Mode")

        # OUTPUT area
        posxget_value = DoubleVar()
        posxget_value.set(Variables.advancedservo.getPosition(0))
        posxget_result = Entry(Variables.frame, textvariable=posxget_value)
        posxget_result.grid(row=9, column=1, sticky=W)

        posyget_value = DoubleVar()
        posyget_value.set(Variables.advancedservo.getPosition(7))
        posyget_result = Entry(Variables.frame, textvariable=posyget_value)
        posyget_result.grid(row=10, column=1, sticky=W)

        samplingarea_value = DoubleVar()
        samplingarea_value.set(Variables.samplingarea_x1y1x2y2)
        samplingarea_result = Entry(Variables.frame, textvariable=samplingarea_value, width=25)
        samplingarea_result.grid(row=11, column=1, sticky=W)

        samplingx = Keyboardcontrol.key_samplingx
        samplingy = Keyboardcontrol.key_samplingy

        x = Variables.advancedservo.getPosition(0)
        y = Variables.advancedservo.getPosition(7)

        windowloop = True

        while windowloop:

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    windowloop = False

                if event.type == pygame.KEYDOWN:

                    if event.key == pygame.K_SPACE:
                        windowloop = False

                    if event.key == pygame.K_LEFT:
                        x -= 1
                    if x < 0:
                        x = 0
                    Variables.advancedservo.setPosition(0, x)

                    if event.key == pygame.K_RIGHT:
                        x += 1
                    if (x + samplingx) >= 100:
                        x = 100 - samplingx
                    Variables.advancedservo.setPosition(0, x)

                    if event.key == pygame.K_UP:
                        y += 1
                    if y > 100:
                        y = 100
                    Variables.advancedservo.setPosition(7, y)

                    if event.key == pygame.K_DOWN:
                        y -= 1
                    if (y - samplingy) <= 0:
                        y = samplingy
                    Variables.advancedservo.setPosition(7, y)

            window.fill(white)

            texto1 = fuente.render("X:" + str(x) + " Y:" + str(-y + 100), 10, (0, 0, 0))
            pygame.draw.rect(window, green, ((x * 5), ((-y + 100) * 5), (samplingx * 5), (samplingy * 5)))

            texto2 = fuente.render("Current position x:", 10, (0, 0, 0))
            texto3 = fuente.render(str(x), 10, (0, 0, 0))

            texto4 = fuente.render("Current position y:", 10, (0, 0, 0))
            texto5 = fuente.render(str(-y + 100), 10, (0, 0, 0))

            samplingareax2 = x + samplingx
            samplingareay2 = y + samplingy
            samplingarea_x1y1x2y2 = str(x) + "," + str(-y + 100) + " : " + str(samplingareax2) + "," + str(
                samplingareay2)

            texto6 = fuente.render("Sampling Area (x1,y1:x2,y2):", 10, (0, 0, 0))
            texto7 = fuente.render(samplingarea_x1y1x2y2, 10, (0, 0, 0))

            window.blit(texto1, ((x * 5), ((-y + 100) * 5) - 10))
            pygame.draw.rect(window, grey, (500, 0, 220, 500))

            pygame.draw.line(window, black, (500, 0), (500, 500), 1)

            # textbox 1
            window.blit(texto2, (510, 10))
            pygame.draw.rect(window, white, (510, 22, 132, 21), 0)
            pygame.draw.rect(window, black, (510, 22, 132, 21), 1)
            window.blit(texto3, (513, 27))

            # textbox 2
            window.blit(texto4, (510, 45))
            pygame.draw.rect(window, white, (510, 57, 132, 21), 0)
            pygame.draw.rect(window, black, (510, 57, 132, 21), 1)
            window.blit(texto5, (513, 62))

            # textbox 3
            window.blit(texto6, (510, 80))
            pygame.draw.rect(window, white, (510, 92, 207, 21), 0)
            pygame.draw.rect(window, black, (510, 92, 207, 21), 1)
            window.blit(texto7, (513, 97))

            pygame.display.update()

            posxget_value.set(Variables.advancedservo.getPosition(0))
            posyget_value.set(100 - Variables.advancedservo.getPosition(7))

            samplingarea_value.set(samplingarea_x1y1x2y2)

        pygame.quit()