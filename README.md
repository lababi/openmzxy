# README #

OpenMZxy allows the control of Phidget motors for custom sampling/ imaging applications.

### New version 2.0! ###
Changes:
1) OpenMZxy is re-written in modular style. 
2) The GUI was improved. 
3) The motors can be controlled now by the keyboard.

### How do I get set up? ###

The program is written for Python 2.x

Required special libraries: 
1) pygame http://www.pygame.org 
2) phidgets http://www.phidgets.com 

### Contribution ###

Comments and code snipplets are always welcome!

### Contact ###

robert.winkler@bioprocess.org, robert.winkler@ira.cinvestav.mx