#!/usr/bin/perl -w


use strict;

### 
### NAME : Filter_CoupleFiles_V_OpenMS.pl 
###
### AUTHOR: Jose Fabricio Lopez
### 
### VERSION : version 1.0
###
###	version 1.0	Filter_CoupleFiles_V_OpenMS.pl	by Jose Fabricio Lopez
###     
### DESCRIPTION : Makes a Pipeline from ---*.mzData,*.mzXML,*.mzML,*.DTA,*.DTA2D,*.mgf,*.featureXML,*.consensusXML,*.ms2,*.fid,*.tsv,*.peplist,*.kroenik,*.edta--- to .mzML format and couples the RetentionTime
###			whit the best time for each measure.		  
###
### USAGE :
###       ./Filter_CoupleFiles_V_OpenMS.pl -InputFolder /FoderWheremzXMLFilesAreLocated/ -OutputFolder /OutputFolder/ 
###
### EXAMPLE
###
###    ./Filter_CoupleFiles_V_OpenMS.pl -InputFolder /home/user/Documents/mzXMLFiles/ -OutputFolder /home/user/Documents/mzXMLFiles/OutputFolder/
###
###
### OPTIONS :
###	-InputFolder	Folder Where Al Input Files Are Located
###	-OutputFolder	Output Folder (In Case Of Existing Folder Will Be Overwritten)
###	-h              Print Help
### REQUIREMENTS :
###
###

=pod

=head1 NAME

	 Filter_CoupleFiles_V_OpenMS.pl

					   2013 by Jose Fabricio Lopez jlhernandez@langebio.cinvestav.mx


=head1 VERSION

	1.0

=head1 DESCRIPTION

Makes a Pipeline from .mzXML files to *.mzML

=head1 INPUT FORMAT

File in *.mzData,*.mzXML,*.mzML,*.DTA,*.DTA2D,*.mgf,*.featureXML,*.consensusXML,*.ms2,*.fid,*.tsv,*.peplist,*.kroenik,*.edta files

=head1 OUTPUT FORMAT

Files in .mzML and .dat ready to be loaded in imzMLConverter.

=head1 INPUT OPTIONS

=over 2

=item B<-InputFolder>

Folder Where mzXML and *.dat Files Are Located and Labeled

=item B<-OutputFolder>

Folder where output will be saved

=item B<-h>

Help on line

=back

=head1 EXAMPLE(S)

   ./Filter_CoupleFiles_V_OpenMS.pl -InputFolder /home/user/Documents/mzXMLFiles/ -OutputFolder /home/user/Documents/OutputmzXMLFiles/

=head1 REQUIREMENTS

=cut



use Cwd;
my $dir = getcwd;
use Cwd 'abs_path';
my $ScriptPath = abs_path($0);

my ($OpenMSFolder,$FolderFilesToConvert,$OutputFolder);
my @FilesToConvert;

 $ScriptPath=~ s/Filter_CoupleFiles_V_OpenMS\.pl//;

use Getopt::Long;
my %opts = ();
GetOptions (\%opts,'InputFolder=s',
		   'OutputFolder=s',
		   'h|help');

if(scalar(keys(%opts)) != 2){
	print "\n\tusage:	Filter_CoupleFiles_V_OpenMS.pl [options]\n\n";
	print "\t-InputFolder	Folder Where mzXML and *.dat Files Are Located and Labeled. Location must be finished by '/' \n";
	print "\t-OutputFolder	Folder where output will be saved. Location must be finished by '/'n";
	exit;
}

if($opts{'h'}){
	&PrintHelp;
}


$FolderFilesToConvert=$opts{InputFolder};											#Using folder where Files to be transformed are located
$OutputFolder=$opts{OutputFolder};												#Folder where outcomes will appear.



#############################Pipeline#############################

my ($EachFile,$Line);
system("ls $FolderFilesToConvert  >  $FolderFilesToConvert"."FilesToConvert.txt");    						#Creating a File wher all Files to be transfored are in list.
print("$dir\n\n");

open(IN, "$FolderFilesToConvert"."FilesToConvert.txt");		   								#Using the list names file to convert each file.
while(<IN>){
$EachFile=$FolderFilesToConvert.$_;												#Usign complete file information (Including Path)
chomp($EachFile);
	if($_=~ /(.*mzData$|.*mzXML$|.*mzML$|.*DTA$|.*DTA2D$|.*mgf$|.*featureXML$|.*consesnsusXML$|.*ms2$|.*fid$|.*tsv$|.*peplist$|.*kroenik$|.*edta$)/)
	{
	print("$1\n");
$EachFile=$FolderFilesToConvert.$1;
	open (OUT,">$dir/PipelineMZML.1.toppas");										#Creating a temporal file to use it as a blue print for conversion
	open(INI,"$ScriptPath"."PipelineMZML.toppas");										#Using the original blue print transformation file to change it and save it in copy.
		while(<INI>){
		$_=~ s/PUT_FILE_HERE/$EachFile/;										#Changing the input of Pipeline with each file
		$Line=$_;
		print OUT "$Line";
		}
	close(INI);
	close(OUT);
	system("ExecutePipeline -in $dir/PipelineMZML.1.toppas -out_dir $OutputFolder");					#Executing pipeline for each file.
	}
#system("rm $dir/PipelineMZML.1.toppas");											#Removing Pipeline, leaving blue print pipeline.	
}
close(IN);
##############################################################################################################################################################################


###############################################################Using DTA information##########################################################################################
system("ls $OutputFolder"."TOPPAS_out/005 > $FolderFilesToConvert"."Files.mzML.txt");						#Creating a temporal File were all .mzML names files are located.
system("mkdir $OutputFolder/DTAExtractorResults/"); 										#Creating a folder where DTAExtractor will deposit files.
open(IN, "$FolderFilesToConvert"."Files.mzML.txt");										#Running for each .mzML file.
my ($RetentionTime,$MatchingLabel,$veamos);
while(<IN>)
{
chomp($_);
	if($_=~ /^(\d+)/)
	{
	$MatchingLabel=$1;
	}
system("DTAExtractor -out $OutputFolder"."DTAExtractorResults/DTA -in $OutputFolder"."TOPPAS_out/005/$_");
			#Using DTAExtractor to get Retention Times from Mass spectrometer.
print("\n\n\n\n$_\n\n\n\n\n");

	open(INI,"$ScriptPath"."MSVs2D.V1.R");											#File or MSVs2D.V1.R which makes the coupling
	open(OUT,"> ./MSVs2D.1.R");												#Generating new file where informationg will be coupled
	while(<INI>)
	{
	$_=~ s/PUT\_DTA\_PATH_HERE/ls $OutputFolder\DTAExtractorResults\//g;							#Changing path in R file where outcomes of DTAExtractor are
	$_=~ s/PUT\_FILE\_2D\_HERE/$FolderFilesToConvert$MatchingLabel\*.dat/g;							#Using .dat file generated by 2D robot.
	$_=~ s/OUTPUT_FILE/$OutputFolder\TOPPAS\_out\/005\/$MatchingLabel/g;							#Where output files will be displayed.
	$_=~ s/PUT_CODE_HERE/$FolderFilesToConvert$MatchingLabel/g;								#Uses de Matching label to know the corresponding file
	print(OUT $_);
	}
	close(INI);
	close(OUT);
system("R < ./MSVs2D.1.R --no-save");												#Runing R script
system("rm ./MSVs2D.1.R");													#Deleting R script, leaving blue print of R script.
system("rm $OutputFolder"."DTAExtractorResults/*.dta");										#Removing folder used
}
system("rm $FolderFilesToConvert*txt");
system("rmdir $OutputFolder"."DTAExtractorResults/"); 										#Removing the folder where DTAExtractor will deposit files.
system("mv $OutputFolder"."TOPPAS_out/005/* $OutputFolder");
system("rmdir $OutputFolder"."TOPPAS_out/005/; rm $OutputFolder"."TOPPAS.log; rmdir $OutputFolder"."TOPPAS_out/");
##################################################################################################################################################################################


##################################################################
#### Displays information when -h o -help is fetched  	        ##
##################################################################
sub PrintHelp {
	system "pod2text -c $0 ";
	exit()
}
