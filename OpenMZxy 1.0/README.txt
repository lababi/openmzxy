OpenMZxy is a software for controlling an imaging/sampling system based
on Phidgets. A PhidgetAdvancedServo 8-Motor controller, and a Joystick
(of good quality, such as a flight simulation joystick) need to be 
connected for the function of the program. 2D movement of a sample carrier
is implemented and requires 2 Linear Actuator L12-100-100-06-R (Phidgets Inc.).

Copyright (C) 2013 Dr. Robert Winkler 
email: robert.winkler@ira.cinvestav.mx, robert.winkler@bioprocess.org 
CINVESTAV Unidad Irapuato
Km. 9.6 Libramiento Norte Carr. Irapuato-Le�n
36821 Irapuato Gto., M�xico
Tel.: +52-462-6239-635
http://www.ira.cinvestav.mx/lababi.aspx

Contributor(s): Pedro Jim�nez-Sandoval

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
