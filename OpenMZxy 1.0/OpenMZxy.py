# coding: utf-8

#    OpenMZxy is a software for controlling an imaging/sampling system based
#    on Phidgets. A PhidgetAdvancedServo 8-Motor controller, and a Joystick
#    (of good quality, such as a flight simulation joystick) need to be 
#    connected for the function of the program. 2D movement of a sample carrier
#    is implemented and requires 2 Linear Actuator L12-100-100-06-R (Phidgets Inc.).

#    Copyright (C) 2013 Dr. Robert Winkler 
#    email: robert.winkler@ira.cinvestav.mx, robert.winkler@bioprocess.org 
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx

#    Contributor(s): Pedro Jiménez-Sandoval

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


#Phyton specific imports

import pygame, sys, math
from Tkinter import *
from ctypes import *
from time import sleep
from time import strftime
import tkMessageBox

# Phidget specific imports

from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, CurrentChangeEventArgs, PositionChangeEventArgs, VelocityChangeEventArgs
from Phidgets.Devices.AdvancedServo import AdvancedServo
from Phidgets.Devices.Servo import ServoTypes

# Definition of functions

def DisplayControllerInfo():  
 
    print("|------------|---------------------------------------------|--------------|------------|")
    print("|- Attached -|-                   Type                    -|- Serial No. -|-  Version -|")
    print("|------------|---------------------------------------------|--------------|------------|")
    print("|- %8s -|- %30s -|- %10d -|- %8d -|" % (advancedServo.isAttached(), advancedServo.getDeviceName(), advancedServo.getSerialNum(), advancedServo.getDeviceVersion()))
    print("|------------|---------------------------------------------|--------------|------------|")
    print("Number of motors: %i\n" % (advancedServo.getMotorCount()))
  
def TestMotors():
    
    print("Move motors to position PositionMin...\n")
    advancedServo.setPosition(0, advancedServo.getPositionMin(0))
    advancedServo.setPosition(7, advancedServo.getPositionMin(7))
    sleep(10)

    print ("PositionMin reached!\n")
    
    print("Move motors to position PositionMax...\n")
    advancedServo.setPosition(0, advancedServo.getPositionMax(0))
    advancedServo.setPosition(7, advancedServo.getPositionMax(7))
    sleep(10)
    
    print("PositionMax reached!\n \nTesting Finished.")
    
def RectangularSampling():
    
    pygame.init()
    servoposX=int(advancedServo.getPosition(0)) 
    servoposY=int(advancedServo.getPosition(7)) 
    servoposY=100-servoposY          
 
    SamplingCursorX_1=5*servoposX   
    SamplingCursorY_1=5*servoposY

    samplingX=samplingxvalue.get() 
    samplingY=samplingyvalue.get()

    samplingareaX2=servoposX+samplingX 
    samplingareaY2=servoposY+samplingY
    samplingareaX1Y1X2Y2=str(servoposX)+ ","+ str(servoposY) + " : " +str(samplingareaX2)+ ","+ str(samplingareaY2)
    samplingarea_value.set(samplingareaX1Y1X2Y2)
    
    samplingboxX_1=SamplingCursorX_1
    samplingboxY_1=SamplingCursorY_1  
    samplingboxX_2=5*samplingareaX2
    samplingboxY_2=5*samplingareaY2

    square = canvas.create_rectangle(samplingboxX_1,samplingboxY_1,samplingboxX_2,samplingboxY_2, fill="green") 
    canvas.update()
    
    minx=servoposX
    maxx=servoposX+samplingX
    
    if maxx > 95: #Accuracy Range
        maxx=95
    
    #read in data from GUI
    stepperx=int(steppervalue_x.get())
    
    miny=servoposY 
    maxy=servoposY+samplingY 
    
    if maxy > 95: #Accuracy Range
        maxy=95
    
    #read in data from GUI
    steppery=int(steppervalue_y.get())
      
    maxx=maxx+stepperx
    maxy=maxy+steppery
    xcounter=minx
    ycounter=miny 

    #Timing of sampling
    sleeptime = timervalue_1 
    
    #start logging coordinates
    posfilename=strftime("%Y%m%d_%H%M%S") + ".dat"
    posfile = open(posfilename, "a")
    posfilename_value.set(posfilename)
    pyTimeStartSampling=pygame.time.get_ticks()
    
    pyNextPermitTime=float()
    pyNextPermitTime = (timervalue_1.get() + timeinterscan_1.get())* 1000
    
    SleepTimeNew = timervalue_1.get() + timeinterscan_1.get()
    TrueTime = pygame.time.get_ticks()/1000
       
    xdirection=-1
    pygame.init()

       
    for ycounter in range(miny,maxy,steppery): 
        posysetting=100-ycounter 

        advancedServo.setPosition(7, posysetting) 
        servoposX=int(advancedServo.getPosition(0)) 
        servoposY=int(advancedServo.getPosition(7)) 
        servoposY=100-servoposY
                
        SamplingCursorX_2=5*servoposX
        SamplingCursorY_2=5*servoposY
        canvas.create_line(SamplingCursorX_1,SamplingCursorY_1,SamplingCursorX_2,SamplingCursorY_2, width=5, fill="red")
        SamplingCursorX_1=SamplingCursorX_2
        SamplingCursorY_1=SamplingCursorY_2
            
        xdirection=-1*xdirection
            
        posxget_value.set(servoposX)
        posyget_value.set(servoposY)
        canvas.update()
            
        for xcounter in range(minx,maxx,stepperx):
            if xdirection > 0: 
                posxsetting=xcounter 
                print("forward sampling")
            if xdirection < 0:
                posxsetting=maxx-(xcounter-minx+stepperx)
                print("backward sampling")
                
            advancedServo.setPosition(0, posxsetting)
            print("Next SleepTime")
            print(SleepTimeNew)
               
            sleep(SleepTimeNew)
                                                    
            servoposX=int(advancedServo.getPosition(0))
            servoposY=int(advancedServo.getPosition(7))
            servoposY=100-servoposY
                
            if timecheck.get() == 1:
                xyoutput = str(pygame.time.get_ticks() - TrueTime*1000) + "\t" + str(servoposX) + "\t" + str(servoposY) + "\n"
            if timecheck.get() == 0:
                xyoutput = str(servoposX) + "\t" + str(servoposY) + "\n"
                          
            posfile.write(xyoutput)
            SamplingCursorX_2=5*servoposX
            SamplingCursorY_2=5*servoposY
            canvas.create_line(SamplingCursorX_1,SamplingCursorY_1,SamplingCursorX_2,SamplingCursorY_2, width=5, fill="red")
            SamplingCursorX_1=SamplingCursorX_2
            SamplingCursorY_1=SamplingCursorY_2
          
            posxget_value.set(servoposX)
            posyget_value.set(servoposY)

            print("Current Time")
            print(pygame.time.get_ticks()/1000)
                
            pyNextPermitTime = pyNextPermitTime + (timervalue_1.get() + timeinterscan_1.get()) *1000 #Time to Next Position

            SleepTimeNew = ( pyNextPermitTime/1000 - pygame.time.get_ticks()/1000 + TrueTime) 

            print ("NextPermitTime")
            print(pyNextPermitTime/1000)
                               
            canvas.update()

    print("Ready!")
    readymessage="Positioning data have been recorded in " + posfilename
    tkMessageBox.showinfo('Ready!', readymessage)
    posfile.close()

def help_usage():
    tkMessageBox.showinfo('Help',"Read the README file \nJust try, it's easy!")

def help_about():
    tkMessageBox.showinfo('About',"OpenMZxy 1.0 \nLicense: GPLv3 \nRobert Winkler, Pedro Jiménez-Sandoval\nCINVESTAV Irapuato, Mexico, 2013\nhttp://www.ira.cinvestav.mx/lababi.aspx")

def JoystickInit():
    #Number of Joysticks
    numJoysticks = pygame.joystick.get_count()
    print('Number of Joysticks: ', numJoysticks)

    #Creation of object joystickInput from Joystick 1
    global joystickInput
    joystickInput=pygame.joystick.Joystick(0)

    #Initialization of joystickInput and Output of Information
    joystickInput.init()
    jstickPresent=joystickInput.get_init()
    print('Joystick 1, Initialised? ', jstickPresent)
    jstickName = joystickInput.get_name()
    print('Joystick 1, Name: ', jstickName)
    jstickNumAxes=joystickInput.get_numaxes()
    print('Joystick 1, Number of Axes: ', jstickNumAxes)
    jstickNumballs   = joystickInput.get_numballs()
    print('Joystick 1, Number of Balls: ', jstickNumballs)
    jstickNumbuttons = joystickInput.get_numbuttons()
    print('Joystick 1, Number of Buttos: ', jstickNumbuttons)
    jstickNumhats    = joystickInput.get_numhats()
    print('Joystick 1, Number of Hats: ', jstickNumhats)
    global joystickPositionX
    joystickPositionX = joystickInput.get_axis(0)
    global joystickPositionY
    joystickPositionY = joystickInput.get_axis(1)
    print('Joystick Postition X,Y: ', joystickPositionX, joystickPositionY)
    global pyTime
    pyTime=pygame.time.get_ticks()
    print('PyGameTime:', pyTime, 'ms')
    print('************************************************************')

def JoystickControl():
    
    #set Joystick and Canvas drawing position to motor position
    #The Canvas window is 500 x 500, the servo moter has a scale from 0-100, therefore a factor of 5 is used for conversions
    
    servoposX=int(advancedServo.getPosition(0))
    servoposY=int(advancedServo.getPosition(7))
    servoposY=100-servoposY
     
    samplingX=samplingxvalue.get()
    samplingY=samplingyvalue.get()
     
    joystickCursorX_1=5*servoposX
    joystickCursorY_1=5*servoposY
    
    joystickCursorX_2=joystickCursorX_1
    joystickCursorY_2=joystickCursorY_1
    print("Servopos X,Y, Cursor X1, X2:", servoposX, servoposY, joystickCursorX_1,joystickCursorY_1)
    OpointcorrectionX=0
    OpointcorrectionY=0
    Dynamicscorrection=3
    
    print('Using calibrataion set X: ', OpointcorrectionX)
    print('Using calibrataion set Y: ', OpointcorrectionY)

    tkMessageBox.showinfo('Joystick Control',"Lock position by pressing any Joystick-Button")
    
    while True:
        
        print('Reading from Joystick; quit with Joystick-Button!')
        pyTime=pygame.time.get_ticks()
        
        print('PyGameTime:', pyTime, 'ms')
        joystickEvents = pygame.event.get()
        joystickPositionX = (joystickInput.get_axis(0) + OpointcorrectionX) * Dynamicscorrection
        joystickPositionX = round(joystickPositionX,1)
        joystickPositionY = (joystickInput.get_axis(1) + OpointcorrectionY) * Dynamicscorrection
        joystickPositionY = round(joystickPositionY,1)
        
        print('Joystick Postition X,Y: ', joystickPositionX, joystickPositionY)
        canvas.create_line(joystickCursorX_1,joystickCursorY_1,joystickCursorX_2,joystickCursorY_2, width=5, fill="blue")
        joystickCursorX_2=joystickCursorX_1
        joystickCursorY_2=joystickCursorY_1
        joystickCursorX_1=joystickCursorX_1+joystickPositionX
        joystickCursorY_1=joystickCursorY_1+joystickPositionY
        
        joystickServoPosX=joystickCursorX_2/5
        if joystickServoPosX < 0:
            joystickServoPosX=0
        if joystickServoPosX > 100:
            joystickServoPosX=100
        advancedServo.setPosition(0, joystickServoPosX)
        joystickServoPosY=100-(joystickCursorY_2/5)
        if joystickServoPosY < 0:
            joystickServoPosY=0
        if joystickServoPosY > 100:
            joystickServoPosY=100
        advancedServo.setPosition(7, joystickServoPosY)
        
        servoposX=int(advancedServo.getPosition(0))
        servoposY=int(advancedServo.getPosition(7))
        servoposY=100-servoposY
        
        posxget_value.set(servoposX)
        posyget_value.set(servoposY)
        
        samplingareaX2=servoposX+samplingX
        samplingareaY2=servoposY+samplingY
        samplingareaX1Y1X2Y2=str(servoposX)+ ","+ str(servoposY) + " : " +str(samplingareaX2)+ ","+ str(samplingareaY2)

        samplingarea_value.set(samplingareaX1Y1X2Y2)
        
        canvas.update()

        # Loop until Joystick button is pressed
        
        e = pygame.event.wait()
        if e.type == pygame.JOYBUTTONDOWN:
            tkMessageBox.showinfo('Joystick Control',"Button pressed, position locked!")
            print('Button pressed, Stopping!')
            return 0
        if e.type == pygame.JOYAXISMOTION:
            continue

def Quit_OpenMZxy():
    advancedServo.closePhidget()
    quit()

def ManualJoystick():
    pygame.init()
    JoystickInit()
    JoystickControl()

#Definition of global variables
global advancedServo
advancedServo = AdvancedServo()

#Create an advancedServo object

try:
	advancedServo.openPhidget()
	advancedServo.waitForAttach(10000)
	print ("Servo controller %d attached!" % (advancedServo.getSerialNum()))
	DisplayControllerInfo()
    
	print("Setting the servo type for motors 0 and 1 to FIRGELLI_L12_100_100_06_R")
	advancedServo.setServoType(0, ServoTypes.PHIDGET_SERVO_FIRGELLI_L12_100_100_06_R)
	advancedServo.setServoType(7, ServoTypes.PHIDGET_SERVO_FIRGELLI_L12_100_100_06_R)
		
	print("Engage the motors...")
	advancedServo.setEngaged(0, True)
	advancedServo.setEngaged(7, True)
	sleep(2)
		
	print("Engaged state motor x: %s" % advancedServo.getEngaged(0))
	print("Engaged state motor y: %s" % advancedServo.getEngaged(7))

except:
	print("Please check, if the Phidget controller is properly connected.")

# Main Window

root=Tk()
root.title("OpenMZxy 1.0")

# GUI

features=Frame(root)
features.grid(sticky=W)

# INPUT area

header_label = Label(features, text="INPUT")
header_label.grid(row=1, column=2, sticky=W)

timer_label = Label(features, text="Scan time [s]:") # Measuring time on sample position
timer_label.grid(row=3, column=1, sticky=W)

timervalue_1= DoubleVar ()
timervalue_1.set(2.0)

timervalue_entry = Entry(features, textvariable=timervalue_1)
timervalue_entry.grid(row=3, column=2, sticky=W)

interscan_label = Label(features, text="Inter-scan time [s]:")
interscan_label.grid(row=4, column=1, sticky=W)

timeinterscan_1= DoubleVar ()
timeinterscan_1.set(0.5)

timeinterscan_entry = Entry(features, textvariable=timeinterscan_1)
timeinterscan_entry.grid(row=4, column=2, sticky=W)

stepperx_label = Label(features, text="Resolution x-axis [mm]:")
stepperx_label.grid(row=5, column=1, sticky=W)

steppervalue_x=IntVar()
steppervalue_x.set(2)
steppervalue_entry = Entry(features, textvariable=steppervalue_x)
steppervalue_entry.grid(row=5, column=2, sticky=W)

steppery_label = Label(features, text="Resolution y-axis [mm]:")
steppery_label.grid(row=6, column=1, sticky=W)

steppervalue_y=IntVar()
steppervalue_y.set(2)
steppervalue_entry = Entry(features, textvariable=steppervalue_y)
steppervalue_entry.grid(row=6, column=2, sticky=W)

samplingx_label = Label(features, text="Sampling x-axis (to the right) [mm]:")
samplingx_label.grid(row=7, column=1, sticky=W)

samplingxvalue=IntVar()
samplingxvalue.set(10)
samplingxvalue_entry = Entry(features, textvariable=samplingxvalue)
samplingxvalue_entry.grid(row=7, column=2, sticky=W)

samplingy_label = Label(features, text="Sampling y-axis (downwards) [mm]:")
samplingy_label.grid(row=8, column=1, sticky=W)

samplingyvalue=IntVar()
samplingyvalue.set(10)
samplingyvalue_entry = Entry(features, textvariable=samplingyvalue)
samplingyvalue_entry.grid(row=8, column=2, sticky=W)

timecheck=IntVar()
timecheckbox =  Checkbutton (features, text= "Include time [ms] in .dat file", variable = timecheck, onvalue = 1, offvalue = 0, height=2)
timecheckbox.grid(row=9, column=1, sticky=W)
timecheckbox.select()

mscheck=IntVar()
mscheckbox =  Checkbutton (features, text= "Trigger MiD 3500 MS", variable = mscheck, onvalue = 1, offvalue = 0, height=1)
mscheckbox.grid(row=10, column=1, sticky=W)

# OUTPUT area

header_label = Label(features, text="OUTPUT")
header_label.grid(row=11, column=2, sticky=W)

posfilename_label = Label(features, text="t,x,y file:")
posfilename_label.grid(row=12, column=1, sticky=W)

posfilename_value=StringVar()
posfilename_value.set("autogenerated .dat")
posfilename_result = Entry(features, textvariable=posfilename_value)
posfilename_result.grid(row=12, column=2, sticky=W)

try:
	servoposX=int(advancedServo.getPosition(0))
	servoposY=int(advancedServo.getPosition(7))

except:
	servoposX=0
	servoposY=0

servoposY=100-servoposY

posxget_label = Label(features, text="Current position x: ")
posxget_label.grid(row=13, column=1, sticky=W)

posxget_value=IntVar()
posxget_value.set(servoposX)
posxget_result = Entry(features, textvariable=posxget_value)
posxget_result.grid(row=13, column=2, sticky=W)

posyget_label = Label(features, text="Current position y: ")
posyget_label.grid(row=14, column=1, sticky=W)

posyget_value=IntVar()
posyget_value.set(servoposY)
posyget_result = Entry(features, textvariable=posyget_value)
posyget_result.grid(row=14, column=2, sticky=W)

samplingarea_label = Label(features, text="Sampling Area (x1,y1 : x2,y2): ")
samplingarea_label.grid(row=15, column=1, sticky=W)

samplingX=samplingxvalue.get()
samplingY=samplingyvalue.get()
    
samplingareaX2=servoposX+samplingX
samplingareaY2=servoposY+samplingY
samplingareaX1Y1X2Y2=str(servoposX)+ ","+ str(servoposY) + " : " +str(samplingareaX2)+ ","+ str(samplingareaY2)

samplingarea_value=StringVar()
samplingarea_value.set(samplingareaX1Y1X2Y2)
samplingarea_result = Entry(features, textvariable=samplingarea_value)
samplingarea_result.grid(row=15, column=2, sticky=W)

# Canvas drawing window

canvas=Canvas(width=500, height=500, bg='white')
canvas.grid(row=0, column=3, sticky=E)

# creating a menu

menu = Menu(root)
root.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Exit", command=Quit_OpenMZxy)

samplecarriermenu = Menu(menu)
menu.add_cascade(label="Sample Carrier", menu=samplecarriermenu)
samplecarriermenu.add_command(label="Joystick Control (manual)", command=ManualJoystick)
samplecarriermenu.add_command(label="Rectangular Sampling (auto)", command=RectangularSampling) 
samplecarriermenu.add_command(label="Display Controller Info (console)", command=DisplayControllerInfo) 
samplecarriermenu.add_command(label="Test Full Range of Motors (console)", command=TestMotors) 

helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Usage", command=help_usage)
helpmenu.add_command(label="About...", command=help_about)

root.mainloop()
